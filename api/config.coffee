diskAdapter = require('sails-disk')
mongoAdapter = require('sails-mongo')

module.exports = {

  secret: 'secret'

  port: 8080

  waterline: {
    adapters: {
      'default': diskAdapter,
      disk: diskAdapter,
      mongo: mongoAdapter
    }
    connections: {
      default: {
        adapter: 'disk'
      }
    }
    defaults: {
      migrate: 'alter'
    }
  }

  allowedDomains: [
    '*'
  ]

}
