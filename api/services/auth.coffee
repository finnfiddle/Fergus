_ = require('lodash')
config = require('../config')
helpers = require('../../helpers')

service = {}

service.model = {
  identity: 'auth'
  connection: 'default'
  attributes: {}
}

service.collection = (req) -> req.app.models.user

service.actions = {
  register: (req, resp, next)-> 
    collection = service.collection(req)
    if req.session.user?
      resp.send(403, 'A user is currently logged in.')
    else
      helpers.hashPassword(req.body.password, (err, hashedPassword) ->
        req.body.password = hashedPassword
        collection.create(req.body, (err, user)->
          if err? then resp.send(403, err)
          else
            user = _.omit(user, ['password'])
            req.session.user = user
            resp.json(user)
        )
      )
  login: (req, resp, next)-> 
    collection = service.collection(req)
    if req.session.user?
      resp.send(403, 'A user is currently logged in.')
    else
      req.body ?= {password: ''}
      user = collection.findOne({username: req.body.username})
        .exec((err, user) ->
          if err? then resp.send(403)
          else if not user? then resp.send(403)
          else
            helpers.comparePassword(req.body.password, user.password, (err, res) ->
              if res
                user = _.omit(user, ['password'])
                req.session.user = user
                resp.json(user)
              else 
                resp.send(403)
            )
        )
  logout: (req, resp, next)-> 
    req.session.user = null
    resp.send(200)
}

service.routes = {
  'post register': 'register'
  'post login': 'login'
  'get logout': 'logout'
}

module.exports = service
