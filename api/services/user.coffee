_ = require('lodash')
config = require('../config')
helpers = require('../../helpers')

service = {}

service.collection = (req) -> req.app.models[service.model.identity]

updateOrPatch = (req, resp, next) -> 
  collection = service.collection(req)
  data = req.body
  id = req.params.id
  delete data.id
  collection.update({ id: id }, data, (err, users) ->
    if err? then resp.send(err)
    else 
      users = _.map(users, (user)->_.omit(user, ['password']))
      resp.json(users)
  )

service.model = {
  identity: 'user'
  connection: 'default'
  attributes: {
    username: { type: 'string', unique: true, required: true }
    email: { type: 'email',  unique: true, required: true }
    password: { type: 'string', required: true }
  }
}

service.actions = {
  find: (req, resp, next) -> 
    collection = service.collection(req)
    params = req.query
    query = {}
    query.where = _.omit(params, ['limit', 'skip', 'sort_by', 'sort_order'])
    if params.limit? then query.limit = parseInt(params.limit)
    if params.skip? then query.skip = parseInt(params.skip)
    if params.sort_by? 
      order = params.sort_order ?= 'DESC'
      query.sort = "#{params.sort_by} #{order}"
    collection.find(query).exec((err, users)->
      if err? then resp.send(err)
      else if _.size(users) is 0
        resp.send(404)
      else 
        users = _.map(users, (user)->_.omit(user, ['password']))
        resp.json(users)
    )
  get: (req, resp, next) -> 
    collection = service.collection(req)
    id = req.params.id
    collection.findOne({ id: id }).exec((err, user) ->
      if err? then resp.send(err)
      else if not user?
        resp.send(404)
      else resp.json(_.omit(user, ['password']))
    )
  create: (req, resp, next) -> 
    collection = service.collection(req)
    collection.create(req.body, (err, user)->
      if err? then resp.send(err)
      else resp.json(_.omit(user, ['password']))
    )
  update: updateOrPatch
  patch: updateOrPatch
  remove: (req, resp, next) -> 
    collection = service.collection(req)
    collection.destroy({ id: req.params.id }, (err) -> 
      if err? then resp.send(err)
      else resp.send(200)
    )
}

service.routeFilters = {
  '*': ['logged_in']
}

module.exports = service
