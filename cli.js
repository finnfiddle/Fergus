#!/usr/bin/env node

require('coffee-script/register');

program = require('commander'),
  cwd = process.cwd(),
  // _ = require('lodash'),
  fs = require('fs'),
  S = require('string'),
  exec = require('child_process').exec;

program
  .version('0.0.1');

program
  .command('new [name]')
  .description('')
  .action(function(name){
    exec("cd "+cwd+" && git clone https://github.com/finnfiddle/RESTeasy.git "+name+" && cd "+name+" && npm install", function(){console.log(arguments)});
  });

program
  .command('generate [type] [name]')
  .description('generate new service/...')
  .action(function(type, name) {
    var cwd = process.cwd();
    switch (type){
      case 'service':
        fs.readFile('./generate/templates/service.coffee', 'utf8', function(err, data) {
          newFileData = S(data).template({name: name}, '{{', '}}').s;
          fs.writeFile(cwd + "/api/services/"+name+".coffee", newFileData, function()  {
            console.log("Generated model, routes and actions for "+name+" service");
          });
        });
        break;
    }
  })

// program
//   .command('start')
//   .description('start server')
//   .action(function() {
//     var server = require(cwd+'/server');
//     server();
//   })

program
  .command('*')
  .action(function(env)  {
    console.log('Enter a Valid command');
    terminate(true);
  })

program.parse(process.argv)
