Fergus
======

> Light-weight REST API framework, shipped with basic auth, generator and [Waterline ORM](https://github.com/balderdashy/waterline). Written in [coffeescript](http://coffeescript.org).

### Why Fergus?

Fergus's aim is to enable anybody to get a RESTful API up and running in a matter of minutes. Based on ([express](http://expressjs.com)) with cli service generators that generate all basic CRUD functionality as well as the [Waterline ORM](https://github.com/balderdashy/waterline) from [Sails.js](http://sailsjs.org). It is also easy to add custom endpoints and route filters (middleware). A users service and basic authentication come with a fresh install.

Perfect for rapid prototyping of single-page apps but also able to scale nicely with [adapters for different storage types](https://github.com/balderdashy/waterline#overview) (mongodb, mysql, etc).

No functionality of Waterline or express has been curtailed. The framework can be extended as you wish and modules can be plugged in as middleware on routes.

### Installation 

```bash
npm install fergus -g
```

### Creating a new project

```bash
fergus new app_name
cd app_name
```

### Starting a server

```bash
fergus start
```

### Generating a service

```bash
fergus generate service service_name
```

For example: 

```bash
fergus generate service car
```

Would create a car model and the corresponding CRUD actions and RESTful routes at ```api/services/car.coffee```

### To do

* User roles
* Update/patch for multiple objects at once
* Environments

### Contributing

Coffeescript please. Send pull-requests my way. Any feedback is more than welcome.
