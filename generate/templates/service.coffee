_ = require('lodash')
config = require('../config')
helpers = require('../../helpers')

service = {}

service.collection = (req) -> req.app.models[service.model.identity]

service.model = {
  identity: '{{name}}'
  connection: 'default'
  attributes: {
    # name: { type: 'string', unique: true, required: true }
  }
}

service.actions = {
  find: (req, resp, next) -> 
    collection = service.collection(req)
    params = req.query
    query = {}
    query.where = _.omit(params, ['limit', 'skip', 'sort_by', 'sort_order'])
    if params.limit? then query.limit = parseInt(params.limit)
    if params.skip? then query.skip = parseInt(params.skip)
    if params.sort_by? 
      order = params.sort_order ?= 'DESC'
      query.sort = "#{params.sort_by} #{order}"
    collection.find(query).exec((err, things)->
      if err? then resp.send(err)
      else if _.size(things) is 0
        resp.send(404)
      else resp.json(things)
    )
  get: (req, resp, next) -> 
    collection = service.collection(req)
    id = req.params.id
    collection.findOne({ id: id }).exec((err, thing) ->
      if err? then resp.send(err)
      else if not thing?
        resp.send(404)
      else resp.json(thing)
    )
  create: (req, resp, next) -> 
    collection = service.collection(req)
    collection.create(req.body, (err, thing)->
      if err? then resp.send(err)
      else resp.json(thing)
    )
  update: helpers.updateOrPatch
  patch: helpers.updateOrPatch
  remove: (req, resp, next) -> 
    collection = service.collection(req)
    collection.destroy({ id: req.params.id }, (err) -> 
      if err? then resp.send(err)
      else resp.send(200)
    )
}

service.routeFilters = {
  '*': []
}

module.exports = service
