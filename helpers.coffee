_ = require('lodash')
fs = require("fs")
bcrypt = require('bcrypt')
config = require('./api/config')
helpers = {}

DEFAULT_ACTIONS = {
  find: (req, resp, next) -> next()
  get: (req, resp, next) ->  next()
  create: (req, resp, next) ->  next()
  update: (req, resp, next) ->  next()
  patch: (req, resp, next) ->  next()
  remove: (req, resp, next) ->  next()
  setup: (app) ->  return
}

helpers.loadFromDir = (dirPath, obj) ->
  isArr = _.isArray(obj)
  dirPath = __dirname + dirPath
  fs.readdirSync(dirPath).forEach((file) ->
    if isArr then obj.push(require("#{dirPath}/#{file}"))
    else 
      fileName = file.split('.')[0]
      obj[fileName] = require("#{dirPath}/#{file}")
  )
  return obj

middleware = helpers.loadFromDir("/api/middleware", {})

# //CORS middleware
helpers.allowCrossDomain = (req, res, next) ->
  res.header('Access-Control-Allow-Origin', config.allowedDomains)
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE')
  res.header('Access-Control-Allow-Headers', 'Content-Type')
  next()


helpers.applyRouteFilters = (router, service) ->
  if service.model? and service.routeFilters?
    id = service.model.identity
    for filter in _.pairs(service.routeFilters)
      uri = "/#{id}"
      if filter[0] is not '*' then uri += "/#{filter[0]}"
      for mw in filter[1]
        router.use(uri, middleware[mw])          

helpers.setupService = (router, service) ->
  if service.model?
    id = service.model.identity
    actions = _.extend(DEFAULT_ACTIONS, service.actions)

    router
    .get("/#{id}/:id", actions.get)
    .get("/#{id}", actions.find)
    .post("/#{id}", actions.create)
    .put("/#{id}/:id", actions.update)
    .patch("/#{id}/:id", actions.update)
    .delete("/#{id}/:id", actions.remove)

    service.routes ?= {}
    for routePair in _.pairs(service.routes)
      method = actions[routePair[1]]
      verbAndRoute = routePair[0].split(' ')
      verb = verbAndRoute[0]
      route = "/#{id}/#{verbAndRoute[1]}"
      switch verb
        when 'all'
          router.use(route, method)
        when 'get'
          router.get(route, method)
        when 'post'
          router.post(route, method)
        when 'put'
          router.put(route, method)
        when 'delete'
          router.delete(route, method)

helpers.updateOrPatch = (req, resp, next) -> 
  collection = service.collection(req)
  data = req.body
  id = req.params.id
  delete data.id
  collection.update({ id: id }, data, (err, users) ->
    if err? then resp.send(err)
    else resp.json(users)
  )

helpers.hashPassword = (pw, done) ->
  bcrypt.genSalt(10, (err, salt) ->
    bcrypt.hash(pw, salt, done)
  )

helpers.comparePassword = bcrypt.compare

module.exports = helpers
