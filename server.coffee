express = require('express')
cookieParser = require('cookie-parser')
bodyParser = require('body-parser')
expressSession = require('express-session')
methodOverride = require('method-override')
_ = require('lodash')
Waterline = require('waterline')
helpers = require('./helpers')
config = require('./api/config')
app = express()
orm = new Waterline()


# load services from services directory
services = helpers.loadFromDir("/api/services", []) 

# // Load the service Models into the ORM
for service in services
  orm.loadCollection(Waterline.Collection.extend(service.model))


# init orm and server



# module.exports = (options)->
# options ?= {}
# config = _.extend(config, options)




orm.initialize(config.waterline, (err, models) ->
  if err 
    throw err

  app.models = models.collections
  app.connections = models.connections

  # scaffold
  # app.models.user.create({first_name: 'John', last_name: 'Smith'}, (err, user) -> return)

  app.use(cookieParser())
  app.use(bodyParser())
  app.use(expressSession({secret: config.secret}))
  app.use(methodOverride())
  app.use(helpers.allowCrossDomain)

  router = express.Router()

  for service in services
    helpers.applyRouteFilters(router, service)
    helpers.setupService(router, service)

  app.use('/', router)    

  # // Start Server
  app.listen(config.port)

  console.log("Listening on port #{config.port}...")

)
